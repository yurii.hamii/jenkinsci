package com.gitlab.yurii.hamii.jencinsci;

/**
 * Hello CI!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        System.out.println( "Hello Jenkins - CI!" );
    }
    
    public static void doNothing() {
    	// do nothing
    }
}
