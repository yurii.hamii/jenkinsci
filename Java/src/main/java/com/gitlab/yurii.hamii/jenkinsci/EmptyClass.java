package com.gitlab.yurii.hamii.jenkinsci;

/**
 * Empty class for Cobertura code coverage
 *
 */
public class EmptyClass {
	
	public static void emptyMethod() {
		// empty
	}
	
	public static void unusedMethod() {
		// do not use it
	}

}
